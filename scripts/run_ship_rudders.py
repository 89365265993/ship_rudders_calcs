import tkinter as tk
import ship_rudders_graphics
import ship_import
import ship_output
import ship_rudders_calculations
import find_path
import kompas_output

Path_to_data = "/data/SDC-1481.s2g"
Path_to_out = "/data/Output"
Path_to_kompas_template = "/data/KOMPAS/template.txt"


def run_tk():
    print('Starting tinker window...\n')


class MainWindow(tk.Frame):
    def __init__(self, master):
        super().__init__(master)
        self.pack()

        self.path_to_the_source_file = find_path.switch_from_to(folder_to=Path_to_data)
        self.path_to_the_out_file = find_path.switch_from_to(folder_to=Path_to_out)
        self.kompas_template = find_path.switch_from_to(folder_to=Path_to_kompas_template)

        self.ship_data = []
        self.diam_butt_coords = []
        self.stern_valance_coords = []
        self.diam_butt_square = None
        self.stern_valance_square = None
        self.propellers_quantity = None
        self.f_0 = None
        self.summer_draft = None
        self.rudder_axe_x = None
        self.waterline_length = None
        self.sigma_k = None
        self.c_m = None
        self.c_b = None
        self.c_p = None
        self.e_1 = None
        self.e_2 = None
        self.underwater_diam_butt_coords = []
        self.underwater_diam_butt_square = None
        self.half_underwater_volume = None
        self.half_breadth = None
        self.max_speed = None
        self.rudder_square = None
        self.h_p = None
        self.a_3 = None
        self.a_4 = None
        self.x_0 = None

        self.frm_tools = tk.LabelFrame(master=master, width=800, text='Import ship data before trying to draw!')
        self.frm_tools.pack(side='left', anchor='n', padx=5, pady=5)

        self.frm_input = tk.Frame(master=master)
        self.frm_input.pack(side='left', anchor='n')

        self.ship_info = tk.LabelFrame(master=master, text='Ship info')
        self.ship_info.pack(side='left', anchor='n', padx=5, pady=5)

        self.frm_output = tk.LabelFrame(master=master, text='Output')
        self.frm_output.pack(side='left', anchor='n', padx=5, pady=5)

        # Frame ship_info
        self.frm_diam_buttock_square = tk.Frame(master=self.ship_info)
        self.frm_diam_buttock_square.pack(side='top', fill=tk.X)
        self.entry_diam_butt_square = tk.Entry(master=self.frm_diam_buttock_square, width=10)
        self.entry_diam_butt_square.pack(side='right', fill=tk.X)
        self.lbl_diam_butt_square = tk.Label(master=self.frm_diam_buttock_square, text='Diam buttock square:')
        self.lbl_diam_butt_square.pack(side='right')

        self.frm_underwater_diam_butt_square = tk.Frame(master=self.ship_info)
        self.frm_underwater_diam_butt_square.pack(side='top', fill=tk.X)
        self.entry_underwater_diam_butt_square = tk.Entry(master=self.frm_underwater_diam_butt_square, width=10)
        self.entry_underwater_diam_butt_square.pack(side='right', fill=tk.X)
        self.lbl_underwater_diam_butt_square = tk.Label(master=self.frm_underwater_diam_butt_square,
                                                        text='Underwater diam buttock square:')
        self.lbl_underwater_diam_butt_square.pack(side='right')

        self.frm_stern_valance_square = tk.Frame(master=self.ship_info)
        self.frm_stern_valance_square.pack(side='top', fill=tk.X)
        self.entry_stern_valance_square = tk.Entry(master=self.frm_stern_valance_square, width=10)
        self.entry_stern_valance_square.pack(side='right', fill=tk.X)
        self.lbl_stern_valance_square = tk.Label(master=self.frm_stern_valance_square, text='Stern valance square:')
        self.lbl_stern_valance_square.pack(side='right')

        self.frm_displacement = tk.Frame(master=self.ship_info)
        self.frm_displacement.pack(side='top', fill=tk.X)
        self.entry_displacement = tk.Entry(master=self.frm_displacement, width=10)
        self.entry_displacement.pack(side='right', fill=tk.X)
        self.lbl_displacement = tk.Label(master=self.frm_displacement, text='Displacement:')
        self.lbl_displacement.pack(side='right')

        self.frm_waterline_length = tk.Frame(master=self.ship_info)
        self.frm_waterline_length.pack(side='top', fill=tk.X)
        self.entry_waterline_length = tk.Entry(master=self.frm_waterline_length, width=10)
        self.entry_waterline_length.pack(side='right', fill=tk.X)
        self.lbl_waterline_length = tk.Label(master=self.frm_waterline_length, text='Length:')
        self.lbl_waterline_length.pack(side='right')

        self.frm_breadth = tk.Frame(master=self.ship_info)
        self.frm_breadth.pack(side='top', fill=tk.X)
        self.entry_breadth = tk.Entry(master=self.frm_breadth, width=10)
        self.entry_breadth.pack(side='right', fill=tk.X)
        self.lbl_breadth = tk.Label(master=self.frm_breadth, text='Breadth:')
        self.lbl_breadth.pack(side='right')

        self.frm_c_m = tk.Frame(master=self.ship_info)
        self.frm_c_m.pack(side='top', fill=tk.X)
        self.entry_c_m = tk.Entry(master=self.frm_c_m, width=10)
        self.entry_c_m.pack(side='right', fill=tk.X)
        self.lbl_c_m = tk.Label(master=self.frm_c_m, text='Cm:')
        self.lbl_c_m.pack(side='right')

        self.frm_c_b = tk.Frame(master=self.ship_info)
        self.frm_c_b.pack(side='top', fill=tk.X)
        self.entry_c_b = tk.Entry(master=self.frm_c_b, width=10)
        self.entry_c_b.pack(side='right', fill=tk.X)
        self.lbl_c_b = tk.Label(master=self.frm_c_b, text='Cb:')
        self.lbl_c_b.pack(side='right')

        self.frm_c_p = tk.Frame(master=self.ship_info)
        self.frm_c_p.pack(side='top', fill=tk.X)
        self.entry_c_p = tk.Entry(master=self.frm_c_p, width=10)
        self.entry_c_p.pack(side='right', fill=tk.X)
        self.lbl_c_p = tk.Label(master=self.frm_c_p, text='Cp:')
        self.lbl_c_p.pack(side='right')

        self.frm_sigma_k = tk.Frame(master=self.ship_info)
        self.frm_sigma_k.pack(side='top', fill=tk.X)
        self.entry_sigma_k = tk.Entry(master=self.frm_sigma_k, width=10)
        self.entry_sigma_k.pack(side='right', fill=tk.X)
        self.lbl_sigma_k = tk.Label(master=self.frm_sigma_k, text='Sigma k:')
        self.lbl_sigma_k.pack(side='right')

        self.frm_e_1 = tk.Frame(master=self.ship_info)
        self.frm_e_1.pack(side='top', fill=tk.X)
        self.entry_e_1 = tk.Entry(master=self.frm_e_1, width=10)
        self.entry_e_1.pack(side='right', fill=tk.X)
        self.lbl_e_1 = tk.Label(master=self.frm_e_1, text='E1:')
        self.lbl_e_1.pack(side='right')

        self.frm_e_2 = tk.Frame(master=self.ship_info)
        self.frm_e_2.pack(side='top', fill=tk.X)
        self.entry_e_2 = tk.Entry(master=self.frm_e_2, width=10)
        self.entry_e_2.pack(side='right', fill=tk.X)
        self.lbl_e_2 = tk.Label(master=self.frm_e_2, text='E2:')
        self.lbl_e_2.pack(side='right')

        # Frame frm_import_ship_params
        self.frm_import_ship_path = tk.LabelFrame(master=self.frm_input, text='Ship path')
        self.frm_import_ship_path.pack(side='top', fill=tk.X, padx=5, pady=5)

        self.frm_source_inp = tk.Frame(master=self.frm_import_ship_path)
        self.frm_source_inp.pack(side='top', fill=tk.X)
        self.entry_source = tk.Entry(master=self.frm_source_inp, width=40)
        self.entry_source.pack(side='right')
        self.entry_source.insert(0, self.path_to_the_source_file)
        self.lbl_source_inp = tk.Label(master=self.frm_source_inp, text='Ship.s2g source:')
        self.lbl_source_inp.pack(side='right', fill=tk.X)

        self.frm_import_ship_params = tk.LabelFrame(master=self.frm_input, text='Set the parameters of the ship')
        self.frm_import_ship_params.pack(side='top', fill=tk.X, padx=5, pady=5)

        self.frm_summer_draft_inp = tk.Frame(master=self.frm_import_ship_params)
        self.frm_summer_draft_inp.pack(side='top', fill=tk.X)
        self.entry_summer_draft = tk.Entry(master=self.frm_summer_draft_inp, width=40)
        self.entry_summer_draft.pack(side='right')
        self.lbl_summer_draft_inp = tk.Label(master=self.frm_summer_draft_inp, text='Summer draft:')
        self.lbl_summer_draft_inp.pack(side='right', fill=tk.X)

        self.frm_rudder_axe_x_inp = tk.Frame(master=self.frm_import_ship_params)
        self.frm_rudder_axe_x_inp.pack(side='top', fill=tk.X)
        self.entry_rudder_axe_x = tk.Entry(master=self.frm_rudder_axe_x_inp, width=40)
        self.entry_rudder_axe_x.pack(side='right')
        self.lbl_rudder_axe_x_inp = tk.Label(master=self.frm_rudder_axe_x_inp, text='X of rudder shaft:')
        self.lbl_rudder_axe_x_inp.pack(side='right', fill=tk.X)

        self.frm_rudder_square_inp = tk.Frame(master=self.frm_import_ship_params)
        self.frm_rudder_square_inp.pack(side='top', fill=tk.X)
        self.entry_rudder_square = tk.Entry(master=self.frm_rudder_square_inp, width=40)
        self.entry_rudder_square.pack(side='right')
        self.lbl_rudder_square_inp = tk.Label(master=self.frm_rudder_square_inp, text='Rudder square:')
        self.lbl_rudder_square_inp.pack(side='right', fill=tk.X)

        self.frm_max_speed_inp = tk.Frame(master=self.frm_import_ship_params)
        self.frm_max_speed_inp.pack(side='top', fill=tk.X)
        self.entry_max_speed = tk.Entry(master=self.frm_max_speed_inp, width=40)
        self.entry_max_speed.pack(side='right')
        self.lbl_max_speed_inp = tk.Label(master=self.frm_max_speed_inp, text='Max speed:')
        self.lbl_max_speed_inp.pack(side='right', fill=tk.X)

        self.frm_propellers_inp = tk.Frame(master=self.frm_import_ship_params)
        self.frm_propellers_inp.pack(side='top', fill=tk.X)
        self.entry_propellers = tk.Entry(master=self.frm_propellers_inp, width=40)
        self.entry_propellers.pack(side='right')
        self.lbl_propellers_inp = tk.Label(master=self.frm_propellers_inp, text='Number of propellers:')
        self.lbl_propellers_inp.pack(side='right', fill=tk.X)

        # Frame frm_tools
        self.btn_import_ship_data = tk.Button(master=self.frm_tools, text='import ship data',
                                              command=self.run_btn_import_ship_data, bg='white')
        self.btn_import_ship_data.pack(fill=tk.X)

        self.btn_draw_diam_buttock = tk.Button(master=self.frm_tools, text='draw diametrical buttock',
                                               command=self.run_btn_draw_diam_buttock)
        self.btn_draw_diam_buttock.pack(fill=tk.X)

        self.btn_draw_frames = tk.Button(master=self.frm_tools, text='draw frames', command=self.run_btn_draw_frames)
        self.btn_draw_frames.pack(fill=tk.X)

        # Frame output

        # Collect diam buttock
        self.lbl_out_path = tk.Label(master=self.frm_output,
                                     text='Write a path to output file:')
        self.lbl_out_path.pack()

        self.entry_out_path = tk.Entry(master=self.frm_output, width=40)
        self.entry_out_path.pack()
        self.entry_out_path.insert(0, self.path_to_the_out_file)

        self.btn_diam_butt_out_file = tk.Button(master=self.frm_output, text='Collect points of diam buttock',
                                                command=self.write_diam_butt_out_file)
        self.btn_diam_butt_out_file.pack(fill=tk.X)

        # Create kompas macro
        self.btn_kompas_macro_file = tk.Button(master=self.frm_output, text='Create KOMPAS macro',
                                               command=self.write_kompas_macro)
        self.btn_kompas_macro_file.pack(fill=tk.X)

    def write_kompas_macro(self):
        try:
            self.btn_kompas_macro_file.bind("<Button-1>", kompas_output.create_document(
                template=self.kompas_template, path_to_out=self.entry_out_path.get(), ship_coords=self.ship_data))
            self.btn_kompas_macro_file.configure(bg='white')
        except FileNotFoundError:
            self.btn_kompas_macro_file.configure(bg='red')
        except ModuleNotFoundError:
            self.btn_kompas_macro_file.configure(bg='red')
        except PermissionError:
            self.btn_kompas_macro_file.configure(bg='red')

    def write_diam_butt_out_file(self):
        try:
            self.btn_diam_butt_out_file.bind("<Button-1>", ship_output.diam_butt_s2g(
                ship_data=self.ship_data, path=self.entry_out_path.get()))
            self.btn_diam_butt_out_file.configure(bg='white')
        except FileNotFoundError:
            self.btn_diam_butt_out_file.configure(bg='red')
        except ModuleNotFoundError:
            self.btn_import_ship_data.configure(bg='red')
        except PermissionError:
            self.btn_diam_butt_out_file.configure(bg='red')

    def get_ship_data(self):
        try:
            self.ship_data = ship_import.import_ship(path=self.entry_source.get())

            self.entry_propellers.configure(bg='white')
            try:
                self.propellers_quantity = int(self.entry_propellers.get())
            except ValueError:
                self.entry_propellers.configure(bg='red')

            self.entry_rudder_axe_x.configure(bg='white')
            try:
                self.rudder_axe_x = int(self.entry_rudder_axe_x.get())
            except ValueError:
                self.entry_rudder_axe_x.configure(bg='red')

            self.entry_rudder_square.configure(bg='white')
            try:
                self.rudder_square = int(self.entry_rudder_square.get())
            except ValueError:
                self.entry_rudder_square.configure(bg='red')

            self.entry_max_speed.configure(bg='white')
            try:
                self.max_speed = int(self.entry_max_speed.get())
            except ValueError:
                self.entry_max_speed.configure(bg='red')

            self.entry_summer_draft.configure(bg='white')
            try:
                self.summer_draft = float(self.entry_summer_draft.get())
            except ValueError:
                self.entry_summer_draft.configure(bg='red')

            self.diam_butt_coords = ship_import.import_diam_butt_coords(ship_data=self.ship_data)
            print('Calculating diametrical buttock square ...')
            self.diam_butt_square = ship_rudders_calculations.calculate_polygon_square(self.diam_butt_coords)
            self.entry_diam_butt_square.delete(0, 'end')
            self.entry_diam_butt_square.insert(0, str(float('{:.3f}'.format(self.diam_butt_square))))

            print('Calculating stern valance ...')
            self.stern_valance_coords = ship_rudders_calculations.get_stern_valance(
                diam_butt_coords=self.diam_butt_coords, summer_draft=self.summer_draft)
            self.stern_valance_square = ship_rudders_calculations.calculate_polygon_square(self.stern_valance_coords)
            self.entry_stern_valance_square.delete(0, 'end')
            self.entry_stern_valance_square.insert(0, str(float('{:.3f}'.format(self.stern_valance_square))))

            print('Calculating length ...')
            self.waterline_length = ship_rudders_calculations.calculate_length(
                diam_butt_coords=self.diam_butt_coords, level=self.summer_draft)
            self.entry_waterline_length.delete(0, 'end')
            self.entry_waterline_length.insert(0, str(float('{:.3f}'.format(self.waterline_length))))

            self.btn_import_ship_data.configure(bg='white')

            print('Collecting underwater diametrical buttock coordinates ...')
            self.underwater_diam_butt_coords = ship_rudders_calculations.get_underwater_diam_butt_coords(
                diam_butt_coords=self.diam_butt_coords, level=self.summer_draft)
            self.underwater_diam_butt_square = ship_rudders_calculations.calculate_polygon_square(
                self.underwater_diam_butt_coords)
            self.entry_underwater_diam_butt_square.delete(0, 'end')
            self.entry_underwater_diam_butt_square.insert(0, str(float('{:.3f}'.format(
                self.underwater_diam_butt_square))))

            print('Calculating Cm ...')
            self.c_m = ship_rudders_calculations.calculate_c_m(ship_data=self.ship_data, level=self.summer_draft)
            self.entry_c_m.delete(0, 'end')
            self.entry_c_m.insert(0, str(float('{:.3f}'.format(self.c_m))))

            print('Calculating Cb ...')
            self.half_underwater_volume = ship_rudders_calculations.calculate_ship_volume(ship_data=self.ship_data,
                                                                                          level=self.summer_draft)
            self.entry_displacement.delete(0, 'end')
            self.entry_displacement.insert(0, str(float('{:.3f}'.format(self.half_underwater_volume * 2))))
            self.half_breadth = ship_rudders_calculations.calculate_breadth(ship_data=self.ship_data,
                                                                            level=self.summer_draft)
            self.entry_breadth.delete(0, 'end')
            self.entry_breadth.insert(0, str(float('{:.3f}'.format(self.half_breadth * 2))))
            self.c_b = ship_rudders_calculations.calculate_c_b(half_volume=self.half_underwater_volume,
                                                               draft=self.summer_draft, half_breadth=self.half_breadth,
                                                               length=self.waterline_length)
            self.entry_c_b.delete(0, 'end')
            self.entry_c_b.insert(0, str(float('{:.3f}'.format(self.c_b))))

            print('Calculating Cp ...')
            self.c_p = ship_rudders_calculations.calculate_c_p(c_m=self.c_m, c_b=self.c_b)
            self.entry_c_p.delete(0, 'end')
            self.entry_c_p.insert(0, str(float('{:.3f}'.format(self.c_p))))

            print('Calculating E1 ...')
            self.f_0 = ship_rudders_calculations.calculate_f_0(propellers=self.propellers_quantity)
            self.sigma_k = ship_rudders_calculations.calculate_sigma_k(self.stern_valance_square, self.waterline_length,
                                                                       self.summer_draft, self.f_0)
            self.entry_sigma_k.delete(0, 'end')
            self.entry_sigma_k.insert(0, str(float('{:.3f}'.format(self.sigma_k))))
            self.e_1 = ship_rudders_calculations.calculate_e_1(c_p=self.c_p, sigma_k=self.sigma_k)
            self.entry_e_1.delete(0, 'end')
            self.entry_e_1.insert(0, str(float('{:.3f}'.format(self.e_1))))

            print('Calculating E2 ...')
            self.h_p = ship_rudders_calculations.calculate_h_p(rudder_axe_x=self.rudder_axe_x,
                                                               diam_butt_coords=self.diam_butt_coords)
            underwater_diam_butt_coords = ship_rudders_calculations.get_underwater_diam_butt_coords(
                diam_butt_coords=self.diam_butt_coords, level=self.h_p)
            self.a_4 = ship_rudders_calculations.calculate_polygon_square(
                underwater_diam_butt_coords)
            self.a_3 = self.diam_butt_square - self.a_4
            self.x_0 = ship_rudders_calculations.calculate_x_0(polygon_coords=underwater_diam_butt_coords,
                                                               z_max=self.h_p)
            self.e_2 = ship_rudders_calculations.calculate_e_2(a_3=self.a_3, a_4=self.a_4, v=self.max_speed,
                                                               h_p=self.h_p, a=self.rudder_square, x_0=self.x_0,
                                                               l_1=self.waterline_length)
            self.entry_e_2.delete(0, 'end')
            self.entry_e_2.insert(0, str(float('{:.3f}'.format(self.e_2))))

        except FileNotFoundError:
            self.btn_import_ship_data.configure(bg='red')

        print('')

    def run_btn_draw_diam_buttock(self):
        try:
            self.btn_draw_diam_buttock.bind("<Button-1>",
                                            ship_rudders_graphics.draw_diametrical_buttock(
                                                self.ship_data, self.diam_butt_coords, self.summer_draft,
                                                self.rudder_axe_x))
        except ModuleNotFoundError:
            self.btn_import_ship_data.configure(bg='red')

    def run_btn_draw_frames(self):
        try:
            self.btn_draw_frames.bind("<Button-1>", ship_rudders_graphics.draw_frames(self.ship_data,
                                                                                      self.summer_draft))
        except ModuleNotFoundError:
            self.btn_import_ship_data.configure(bg='red')

    def run_btn_import_ship_data(self):
        self.btn_import_ship_data.bind("<Button-1>", self.get_ship_data())


if __name__ == "__main__":
    run_tk()
    run_ship_rudders = MainWindow(tk.Tk())
    run_ship_rudders.master.title('Ship rudders')
    run_ship_rudders.mainloop()
