def create_plane(x):
    x1 = str(x).split('.')[0]
    x2 = str(x).split('.')[1]
    command = f'''
iPart7 = kompas_document_3d.TopPart
iPart = iDocument3D.GetPart(kompas6_constants_3d.pTop_Part)

obj = iPart.NewEntity(kompas6_constants_3d.o3d_planeOffset)
iDefinition = obj.GetDefinition()
iDefinition.direction = True
iDefinition.offset = {x}
iPlane = iPart.GetDefaultEntity(kompas6_constants_3d.o3d_planeYOZ)
iDefinition.SetPlane(iPlane)
obj.name = "Plane:{x1}{x2}"
iColorParam = obj.ColorParam()
iColorParam.color = 16776960
obj.Create()

iPart7 = kompas_document_3d.TopPart
iPart = iDocument3D.GetPart(kompas6_constants_3d.pTop_Part)

iPart7 = iKompasDocument3D.TopPart
iModelContainer = kompas_api7_module.IModelContainer(iPart7)
iSketchs = iModelContainer.Sketchs
iSketch = iSketchs.Add()
# iCollection = iPart.EntityCollection(kompas6_constants_3d.o3d_planeOffset)
# iCollection.SelectByPoint(70.898, 0, 0)
# iPlane = iCollection.First()
iPlane = iPart7.DefaultObject(const_3d.o3d_planeXOY)
iSketch.Plane = iPlane
iSketch.Update()


iDocument2D = iSketch.BeginEdit()
iDocument2D = iKompasObject.ActiveDocument2D()

    '''
    return command


def create_point(y, z):
    command = f'''
obj = iDocument2D.ksPoint({y}, {z}, 0)
'''
    return command


def update_sketch():
    command = '''
iSketch.EndEdit()

    '''
    return command


def create_document(template, path_to_out, ship_coords):
    tpl = open(template, 'r')
    path_to_out += '/ship.m3m'
    file = open(path_to_out, 'w')
    print(f'Opening {path_to_out}...')
    print('Dumping...')
    for line in tpl:
        file.write(line)
    for frame in ship_coords:
        command = create_plane(frame['X'])
        for ln in command:
            file.write(ln)
        for point in frame['coordinates']:
            cmd = create_point(point.y, point.z)
            for ln in cmd:
                file.write(ln)
        for string in update_sketch():
            file.write(string)
    tpl.close()
    file.close()
