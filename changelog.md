History
====
Commit Auto-path
----
+ Now the path to the default files is updated automatically, no need to write it manually
----
Commit "#2 E1 full calculation"
----
+ Added calculation of the ship breadth, displacement and coefficients
+ Added calculation for squares of frames
+ Fixed bugs with missing last frame
+ Fixed bug in e_1 calculations
+ Reorganized Ship info block (problems with alignment)
+ Optimisation was made
----
Commit "#2 Ship length"
----
+ Added input of summer draft
+ Added a red waterline on the plot
+ Added calculation of the ship length
+ Fixed bugs with stern valance depended on draft
+ Optimisation was made
----
Commit "#2 Stern valance"
----
+ Added a function for collecting stern valance coordinates
+ Fixed bugs from prev commit
+ Optimisation was made
----
Commit "#2 E1 calculator"
----
+ Added a function for calculating E1
+ Optimisation was made
----
Commit "#2 Square calculator"
----
+ Added a function for calculating square of a polygon
+ Optimisation was made
----
Commit "#2 Diametrical buttock is now sorted"
----
+ Diametrical buttock is sorted and can be printed in lines
+ Optimisation was made
----
Commit "#2 First interaction"
----
+ When user forgets to input something the input button turns red
+ Interaction: user can change path to the ship file
+ Added a section to dump diametrical buttock into .s2g file
+ Better GUI division into frames
----
Commit "Division into files"
----
+ One big script is divided into smaller ones
+ The app script is rewritten in OOP
+ The GUI is divided into frames
+ Added drafts for input of ship parameters
----
Commit "first GUI"
----
+ Now you can input data and draw frames and buttocks with GUI
+ Don't forget to input ship data before drawing
+ Check PATH to the data file before the first use
----
Commit "ship_data sorting"
----
+ list of frames is sorted by its numbers
+ added zero and scale constants
----
Commit "Draw diametrical buttock in points"
----
+ added a function to draw diametrical buttock in points
----
Commit "Creation of s2g reader"
----
+ Filereader for s2g was created
+ Data is displayed in simple graphics
